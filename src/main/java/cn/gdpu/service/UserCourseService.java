package cn.gdpu.service;

import cn.gdpu.bean.SelectableCourse;
import cn.gdpu.bean.UserCourse;
import cn.gdpu.mapper.SelectableCourseDAO;
import cn.gdpu.mapper.UserCourseDAO;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName UserCourseService
 * @Author ttaurus
 * @Date Create in 2020/3/9 14:38
 */
@Service
public class UserCourseService{

    @Autowired
    SelectableCourseDAO selectableCourseDAO;
    @Autowired
    UserCourseDAO userCourseDAO;

    /**
     * 退选课程
     * @param courseId
     * @param username
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Object cancelCourse(Integer courseId,String username){
        Map<String ,Object> map= new HashMap<>();
        try{
            if(userCourseDAO.checkCourse(username,courseId) == null){  //判断是否已选过该课程
                map.put("msg","你已退选该课程!");
                map.put("flag",false);
                return JSON.toJSON(map);
            }
            selectableCourseDAO.updateAddCourseStock(courseId);
            userCourseDAO.delete(courseId,username);
            map.put("msg","退课成功!");
            map.put("flag",true);
            return JSON.toJSON(map);
        }catch(Exception e){
            System.out.println("退选异常！已回滚操作");
            throw e;
        }
    }

    /**
     * 选择课程
     * @param courseId
     * @param username
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Object chooseCourse(Integer courseId,String username){
        Map<String ,Object> map= new HashMap<>();
        try{
            Integer stock = selectableCourseDAO.getStockByCourseId(courseId);
            if(stock <= 0){  //判断课程是否已经无剩余人数
                map.put("msg","该课程已被选完啦!");
                map.put("flag",false);
                return JSON.toJSON(map);
            }
            List<Integer> ids = userCourseDAO.selectByUsername(username);
            if(ids.contains(courseId)){  //判断是否已选过该课程
                map.put("msg","你已选择该课程,请勿重复选择!");
                map.put("flag",false);
                return JSON.toJSON(map);
            }
            if(ids.size() >= 3){
                map.put("msg","每个人至多选择3门课程!");
                map.put("flag",false);
                return JSON.toJSON(map);
            }
            selectableCourseDAO.updateMinCourseStock(courseId); //如果出异常 必定这个地方! 
            UserCourse course = new UserCourse();
            Date date = new Date();
            course.setCourseId(courseId);
            course.setUsername(username);
            course.setSelectTime(date);
            userCourseDAO.insert(course);
            map.put("msg","选课成功!");
            map.put("flag",true);
            return JSON.toJSON(map);
        }catch(Exception e){
            System.out.println("系统繁忙,请稍后再进行尝试!");
            throw e;
        }
    }

    /**
     * 拿到该学生的全部课程
     * @param username
     * @return
     */
    public List<SelectableCourse> selectByUser(String username){
        try{
            return selectableCourseDAO.selectByUser(username);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
